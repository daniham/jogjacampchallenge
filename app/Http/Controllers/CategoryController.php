<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\jogjaCampEmail;
use App\Models\Category;

use Illuminate\Support\Facades\Mail;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::latest()->paginate(8);
      
        return view('categories.index',compact('categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function email(){
 
		Mail::to("testing@devque.com")->send(new jogjaCampEmail());
 
		return "Email telah dikirim";
 
	}

    public function search(Request $request)
    {
        $keyword = $request->search;
        $categories = Category::where('name', 'like', "%" . $keyword . "%")->paginate(5);
        return view('categories.index', compact('categories'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('categories.create');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'is_publish' => 'required',
        ]);
      
        Category::create($request->all());
       
        return redirect()->route('categories.index')
                        ->with('success','Categori created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('categories.show',compact('category'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $categories)
    {
        $request->validate([
            'name' => 'required',
            'is_publish' => 'required',
        ]);
      
        $categories->update($request->all());
      
        return redirect()->route('categories.index')
                        ->with('success','Category updated successfully');
    }

    public function destroy(Category $categories)
    {
        $categories->delete();
       
        return redirect()->route('categories.index')
                        ->with('success','Deleted successfully');
    }
}
