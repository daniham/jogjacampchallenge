<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('students', StudentController::class);
Route::resource('categories', CategoryController::class);
// Route::get('search', [StudentController::class, 'search'])->name('search');
Route::get('search', [CategoryController::class, 'search'])->name('search');
Route::get('kirimemail','App\Http\Controllers\CategoryController@email');

Route::get('email-test', function(){

    $details['email'] = 'danihamdani866@gmail.com';
    
    dispatch(new App\Jobs\SendEmailJob($details));
    
    dd('done');
    });





